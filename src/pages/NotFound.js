import { Link } from "react-router-dom";
import { Col, Container, Row } from 'react-bootstrap'
export default function NotFound() {

  return (
    <Container >
      <Row>
        <Col className='col-md-5 col-10 ms-auto'>
          <div className='p-5 text-center'>
            <h1 className='mt-5 text-light'>Oops! That page can't be found.</h1>
            <p className='text-light'>Go back to the <Link to='/'>homepage</Link></p>
          </div>
        </Col>
      </Row>
    </Container>
  )
}
