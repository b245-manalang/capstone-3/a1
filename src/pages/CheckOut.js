import React from 'react'
import { Button, Form, Row, Col, Container } from 'react-bootstrap';
import { Fragment, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { user, useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CheckOut() {

	const [name, setName] = useState('');
	const [quantity, setQuantity] = useState('');
	const navigate = useNavigate();
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (name !== '' && quantity !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [name, quantity])

	function addToCart(event) {
		event.preventDefault();

		fetch(`http://localhost:4000/order`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				quantity: quantity
			})
		})
			.then(result => result.json())
			.then(data => {
				console.log(data)

				if (data === true) {
					Swal.fire({
						title: 'Order successfully created!',
						icon: 'success',
					})
					navigate("/cart");
				} else {

					Swal.fire({
						title: 'Oops! There was an error in adding a product',
						icon: 'error',
						text: 'Please try again!'
					})
				}
			})
			.catch(err => console.log(err))
	}
	return (
		<Container>
			<Row className="mt-5">
				<Col className='addP col-md-4 col-10 ms-auto p-3'>
					<Fragment>
						<h1 className='text-center'>Add to Cart</h1>
						<Form className='mt-4' onSubmit={event => addToCart(event)}>
							<Form.Group className="mb-3" controlId="formBasicName">
								<Form.Label>Name</Form.Label>
								<Form.Control
									type="name"
									placeholder="Enter product name"
									value={name}
									onChange={event => setName(event.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicQuantity">
								<Form.Label>Quantity</Form.Label>
								<Form.Control
									type="quantity"
									placeholder="Enter quantity"
									value={quantity}
									onChange={event => setQuantity(event.target.value)}
									required
								/>
							</Form.Group>
							{
								isActive ?
									<Button variant="secondary" type="submit" className="mx-auto">
										Submit
									</Button>
									:
									<Button variant="dark" type="submit" disabled>
										Submit
									</Button>
							}
						</Form>
					</Fragment>
				</Col>
			</Row>
		</Container>
	);
}
