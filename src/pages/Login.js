import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { useState, useEffect, useContext, Fragment } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';


export default function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate()
    const { user, setUser } = useContext(UserContext);

    console.log(user);

    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if (email !== "" && password !== "") {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [email, password])

    function login(event) {
        event.preventDefault();

        fetch(`http://localhost:4000/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(result => result.json())
            .then(data => {
                console.log(data)

                if (data === false) {
                    Swal.fire({
                        title: "Authentication failed!",
                        icon: "error",
                        text: "Please try again!"
                    })
                } else {
                    localStorage.setItem('token', data.auth);
                    retrieveUserDetails(localStorage.getItem('token'));

                    Swal.fire({
                        title: "Authentication successful!",
                        icon: 'success',
                        text: "Welcome to JavaSweet!"
                    })

                    navigate('/');
                }
            })
    }

    const retrieveUserDetails = (token) => {
        fetch(`http://localhost:4000/user/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(result => result.json())
            .then(data => {
                console.log(data)

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    return (
        user ?
            <Navigate to="*" />
            :
            <Container>
                <Row className="mt-5">
                    <Col className='loginBg col-md-4 col-10 ms-auto p-3'>
                        <Fragment>
                            <h1 className='text-center'>Login</h1>
                            <Form className='mt-4' onSubmit={event => login(event)}>
                                <Form.Group className="mb-3" controlId="formGroupEmail">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        type="email"
                                        placeholder="Enter email"
                                        value={email}
                                        onChange={event => setEmail(event.target.value)}
                                        required

                                    />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formGroupPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Password"
                                        value={password}
                                        onChange={event => setPassword(event.target.value)}
                                        required />
                                    <Form.Text className="text-muted">
                                        Don't have an account yet? <Link to="/register">Sign up now</Link>
                                    </Form.Text>
                                </Form.Group >
                                {
                                    isActive ?
                                        <Button variant="secondary" className="mx-auto" onClick={event => login(event)}>
                                            Submit
                                        </Button>
                                        :
                                        <Button variant="dark" type="submit" disabled>
                                            Submit
                                        </Button>
                                }
                            </Form>
                        </Fragment>
                    </Col>
                </Row>
            </Container>
    )
}
