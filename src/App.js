
import './App.css';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar';
import Homepage from './pages/Homepage';

import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';

import AddProduct from './products/AddProduct';
import AllProducts from './products/AllProducts';
import ActiveProducts from './products/ActiveProducts';
import Single from './products/RetrieveSingleProduct';
import UpdateProduct from './products/UpdateProduct';
import ArchiveProduct from './products/ArchiveProduct';

import {UserProvider} from './UserContext.js';
import {useState, useEffect} from 'react';
import NotFound from './pages/NotFound';
import Cart from './pages/Cart';
import Dashboard from './admin/Dashboard';
import CheckOut from './pages/CheckOut';


function App() {
  const [user , setUser] = useState(null);

  useEffect(()=>{
    console.log(user)
  },[user])
  
  const unSetUser = () =>{
    localStorage.clear()
}

useEffect(()=>{

fetch(`http://localhost:4000/user/details`,{
  headers:{
      Authorization: `Bearer ${localStorage.getItem('token')}`
  }
})
.then(result => 
  result.json()
).then(data => {
  console.log(data)
  if(localStorage.getItem('token')!== null){
    setUser({
      id:data._id,
      isAdmin: data.isAdmin
  })
  }else{
    setUser(null)
  }
})
},[])

  return (
    <div className='App'>
      <div className='AppBg'></div>
    
    <UserProvider value ={{user, setUser, unSetUser}}>
       <Router>
         <Navbar/>
         <Routes>
             <Route path = "/" element = {<Homepage/>} />
             <Route path = "*" element = {<NotFound/>} />
             <Route path = "/dashboard" element = {<Dashboard/>}/>
             <Route path = "/login" element = {<Login/>} />
             <Route path = "/register" element = {<Register/>}/>
             <Route path = "/logout" element = {<Logout/>}/>

        
             <Route path = "/products" element = {<AllProducts/>}/>
             <Route path = "/products/active" element = {<ActiveProducts/>}/>
             <Route path = "/products/add" element = {<AddProduct/>}/>
             <Route path = "/products/:productId" element = {<Single/>}/>
             <Route path = "/products/update/:productId" element = {<UpdateProduct/>}/>
             <Route path = "/products/archive/:productId" element = {<ArchiveProduct/>}/>

             <Route path = "/cart" element = {<Cart/>}/>
             <Route path = "/order" element = {<CheckOut/>}/>
          
         </Routes>
       </Router>


    </UserProvider>
    </div>
  );
}

export default App;
