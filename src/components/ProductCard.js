import { Row, Col, Button, Card } from 'react-bootstrap';
import { useState, useEffect, useContext, Fragment } from 'react';

import UserContext from '../UserContext.js'
import { Link } from 'react-router-dom';

export default function ProductCard({ productProp }) {

	const { _id, name, description, price } = productProp;
	const [isDisabled, setIsDisabled] = useState(false);
	const { user } = useContext(UserContext);

	return (
		<Row className="mt-5 text-dark bg-none">
			<Col lg={{ span: 6, offset: 3 }}>
				<Card className='card'>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>

						{
							user ?
								<Fragment>
									<Button as={Link} to={`/products/${_id}`} className='cardbtn' disabled={isDisabled}>More details</Button>
								</Fragment>
								:
								<Button as={Link} to="/login">Login</Button>
						}

					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
