import { Row, Col, Button, Card } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext.js'
import { Link } from 'react-router-dom';

export default function OrderCard({ orderProp }) {

	const { name, quantity } = orderProp;
	const [isDisabled, setIsDisabled] = useState(false);
	const { user } = useContext(UserContext);

	return (
		<Row className="mt-5 text-dark bg-none">
			<Col lg={{ span: 6, offset: 3 }}>
				<Card className='card'>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>

						{
							user ?
								<Button as={Link} to={`/order/${_id}`} className='cardbtn' disabled={isDisabled}>Add to Cart</Button>
								:
								<Button as={Link} to="/login">Login</Button>
						}

					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
