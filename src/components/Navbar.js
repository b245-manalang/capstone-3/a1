import { Container, Nav, Navbar } from 'react-bootstrap';

import { Link, NavLink } from 'react-router-dom';
import { useContext, Fragment } from 'react';
import UserContext from '../UserContext.js';
export default function NavBar() {

  console.log(localStorage.getItem('email'));
  const { user } = useContext(UserContext);

  return (
    <Navbar className="navbar navbar-expand-lg navbar-dark">
      <Container>
        <Navbar.Brand as={Link} to='/'>JavaSweet</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
            <Nav.Link as={NavLink} to='/products/active'>Products</Nav.Link>
            <Nav.Link as={NavLink} to='/dashboard'>Dashboard</Nav.Link>
            <Nav.Link as={NavLink} to='/cart'>
              <i className="fa-solid fa-cart-shopping"></i>
            </Nav.Link>
            {
              user ?
                <Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
                :
                <Fragment>
                  <Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
                </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
