import ProductCard from '../components/ProductCard.js';
import { Fragment, useEffect, useState, useContext } from 'react';
import { Container, Row } from 'react-bootstrap';
import UserContext from '../UserContext.js';

export default function Products() {

	const [products, setProducts] = useState([]);
	const { user } = useContext(UserContext);

	useEffect(() => {

		fetch(`http://localhost:4000/products/active`)
			.then(result => result.json())
			.then(data => {
				console.log(data);
				setProducts(data.map(product => {
					return (<ProductCard key={product._id} productProp={product} />)
				}))
			})

	}, []);

	return (
		<Fragment>
			<Container>
				<h3 className='mt-5 text-center text-light'>Products</h3>
				<Row>
					{products}
				</Row>
			</Container>
		</Fragment>
	)
}
