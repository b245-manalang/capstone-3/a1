
import React from 'react'
import { useContext } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import { Col, Container, Row, Button } from 'react-bootstrap'
import UserContext from '../UserContext';

export default function Dashboard() {

  const { user } = useContext(UserContext);

  return (
    user && user.isAdmin
      ?
      <Container >
        <Row>
          <Col className='col-md-5 col-10 ms-auto'>
            <div className='p-5 text-center'>
              <h1 className='mt-5 text-light'>Add and manage your products</h1>
              <p className='text-light'>Get closer to your sale by adding products!</p>

              <Button className="btn btn-space btn-outline-light text-light" as={Link} to="/products/add">Add Product</Button>
              <Button className="btn btn-space btn-outline-light text-light" as={Link} to="/products">View Products</Button>
            </div>
          </Col>
        </Row>
      </Container>
      :
      <Navigate to="*" />

  )
}